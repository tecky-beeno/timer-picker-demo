import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';

function d2(x: number) {
  return x < 10 ? '0' + x : '' + x;
}

function TimePicker(props: {
  name: string;
  value: string;
  onChange: (time: string) => void;
}) {
  let [timePart, halfDay] = props.value.split(' ');
  let parts = timePart.split(':');
  let hour = +parts[0];
  let minute = +parts[1];
  return (
    <>
      <input hidden name={props.name} value={props.value} readOnly />
      <select
        value={hour}
        onChange={(e) => {
          let hour = +((e.target as any) as HTMLInputElement).value;
          let value = d2(hour) + ':' + d2(minute) + ' ' + halfDay;
          props.onChange(value);
        }}
      >
        {new Array(12).fill(0).map((_, i) => (
          <option value={i} key={i}>
            {i}
          </option>
        ))}
      </select>
      <select
        value={minute}
        onChange={(e) => {
          let minute = +((e.target as any) as HTMLInputElement).value;
          let value = d2(hour) + ':' + d2(minute) + ' ' + halfDay;
          props.onChange(value);
        }}
      >
        <option value={0}>0</option>
        <option value={30}>30</option>
      </select>

      <select
        value={halfDay}
        onChange={(e) => {
          let halfDay = ((e.target as any) as HTMLInputElement).value;
          let value = d2(hour) + ':' + d2(minute) + ' ' + halfDay;
          props.onChange(value);
        }}
      >
        <option value="AM">AM</option>
        <option value="PM">PM</option>
      </select>
    </>
  );
}

let win = window as any;

function App() {
  let [startTime, setStartTime] = useState('9:00 AM');
  let [endTime, setEndTime] = useState('5:00 PM');
  let [form, setForm] = useState((undefined as any) as HTMLFormElement);
  let formData = new FormData(form!);
  if (form) {
    win.form = form;
    win.formData = formData;
    console.log();
    console.log(formData.get('startTime'));
    console.log(formData.get('endTime'));
    console.log();
  }
  return (
    <div className="App">
      <form>

      </form>
      <hr/>
      <form
        ref={(form: HTMLFormElement) => {
          if (!form) return;
          setForm(form);
          form.startTime.value = startTime;
          form.endTime.value = endTime;
        }}
      >
        Start: {startTime}
        <br />
        <TimePicker
          name="startTime"
          value={startTime}
          onChange={(time) => {
            // form.startTime = time;
            setStartTime(time);
          }}
        />
        <hr />
        End: {endTime}
        <br />
        <TimePicker name="endTime" value={endTime} onChange={setEndTime} />
        <hr />
        <input
          type="reset"
          onClick={() => {
            setStartTime('9:00 AM');
            setEndTime('5:00 PM');
          }}
        />
      </form>
    </div>
  );
}

export default App;
