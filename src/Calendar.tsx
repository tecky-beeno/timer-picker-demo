import React, { useState } from 'react';
import logo from './logo.svg';
import './Calendar.css';
import { isArrayTypeNode } from 'typescript';

function d2(x: number) {
  return x < 10 ? '0' + x : '' + x;
}

let win = window as any;

let SECOND = 1000;
let MINUTE = SECOND * 60;
let HOUR = MINUTE * 60;
let DAY = HOUR * 24;

function toLastSunday(date: Date) {
  date = new Date(date.getTime());
  while (date.getDay() != 0) {
    date.setTime(date.getTime() - DAY);
  }
  return date;
}
function toNextWeekDay(sunday: Date, weekDay: number) {
  return new Date(sunday.getTime() + weekDay * DAY);
}

type AvalibleTime = {
  weekDay: number; // 0..7
  from: string; // hh:mm
  to: string; // hh:mm
};
type ChoosenTime = {
  date: Date;
  hour: number;
  minute: number;
};

type State = {
  avalibleTimes: AvalibleTime[];
  choosen: ChoosenTime[];
};
let initState: State = {
  avalibleTimes: [
    { weekDay: 0, from: '22:00', to: '23:30' },
    { weekDay: 1, from: '00:00', to: '06:30' },
    { weekDay: 3, from: '11:30', to: '07:30' },
    { weekDay: 5, from: '15:30', to: '17:30' },
  ],
  choosen: [],
};

function App() {
  let [currentTime, setCurrentTime] = useState(new Date());
  let [state, setState] = useState(initState);
  let sunday = toLastSunday(currentTime);
  function getCellState(date: Date, hour: number, minute: number) {
    if (
      state.choosen.some((choosen) => {
        return (
          choosen.date.toDateString() == date.toDateString() &&
          choosen.hour == hour &&
          choosen.minute == minute
        );
      })
    ) {
      return 'C';
    }
    return ' ';
  }
  function clickCell(date: Date, hour: number, minute: number) {
    console.log('clickCell', date, hour, minute);
    setState({
      ...state,
      choosen: [
        ...state.choosen,
        {
          date,
          hour,
          minute,
        },
      ],
    });
  }
  return (
    <div>
      current date:
      <input
        type="date"
        value={currentTime.toISOString()}
        onChange={(e) => {
          let input = e.target as HTMLInputElement;
          setCurrentTime(input.valueAsDate!);
        }}
      />
      <br />
      <br />
      current date:
      {currentTime.toISOString()}
      <br />
      <br />
      sunday:
      {sunday.toISOString()}
      <br />
      <br />
      <table>
        <thead>
          <tr>
            <th>Time</th>
            {new Array(7).fill(0).map((_, i) => (
              <th>* {i}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {new Array(48).fill(0).map((_, i) => {
            let h = Math.floor(i / 2);
            let m = (i % 2) * 30;
            let time = d2(h) + ':' + d2(m);
            return (
              <tr>
                <td>{time}</td>
                {new Array(7).fill(0).map((_, i) => {
                  let date = toNextWeekDay(sunday, i);
                  return (
                    <td onClick={() => clickCell(date, h, m)}>
                      {getCellState(date, h, m)}
                    </td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default App;
